# Installing metafont & metapost on your machine

This guide aims to provide instructions for installing and running metapost on your own machine.

## UNIX systems

The package name that contains mpost / metapost and metafont on unix systems is called `tex-live`

On debian systems you would simply:
``` sudo apt-get install texlive ```

On arch systems, the /extra/ sources offer both `texlive-core` and `texlive-bin` packages that are necessary to run mpost

## Windows

Installing texlive on Windows is documentend here https://www.tug.org/texlive/acquire-netinstall.html. Multiple methods are described here, most recently we tested downloading the .iso file *(! over 3.4GB !)* for example from here http://mirror.koddos.net/CTAN/systems/texlive/Images/

## Mac OSx

`http://www.tug.org/mactex/mactex-download.html` lists a MacTeX.pkg *(! approx 3GB !)* file to download. The osx package manager will help you install the package itself.

# running metapost from the command line prompt

Once you have downloaded and installed the texlive package, you must open a command line prompt and check if the installation worked: `$ mpost`

(Note, the *dollar sing* $ here is a convention signifying the beginning of your command line prompt, or where you can start typing.)

The system response will look somethin like this, if your installation was successful:

```This is MetaPost, version 2.000 (TeX Live 2017/Arch Linux) (kpathsea version 6.2.3)```

if your system does not reply something containing a version number, or seems not to know about anything called mpost, your installation has unfortunately failed

## converting mp to a vector file:

to convert a metapost file (containing metapost code) to a vector based file format, we first need to add a few lines to the header of our .mp or .mpost file:
```
outputtemplate := "%c.svg";
outputformat   := "svg";
```

for example the content of 68.mp might be:

```
outputtemplate := "%c.svg";
outputformat   := "svg";
beginfig(68);
z1 = (2u,4u);
z2 = (0.7u,2u);
z3 = (2.5u,4u);
z4 = (0u,6u);
z5 = (2.5u,2.7u);
z6 = (1u, -1u);
z7 = (1u, 5u);
z8 = (2u, 4.3u);
z9 = (0.7u, 3u);

pickup penrazor scaled 80;
draw z7 -- z6 withcolor (0,0,255);
pickup penrazor scaled 50;
draw z7 ..  z1 .. z6;
pickup penrazor scaled 50;
draw z7 .. z3 .. z6;
pickup penrazor scaled 50;


endfig;
```

then, you can convert mpost code to svgs by running this command on the prompt:

`$ mpost -interaction=bashmode -s outputformat="svg" nomdufichiertex.mp`

This will output 68.svg in the same folder you initiated the command from.

Now you have an svg version of your mpost figure, ready to be used as is, or to be tweaked by different mpost code.
