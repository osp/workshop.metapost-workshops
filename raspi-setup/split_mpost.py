#!/usr/bin/python
# -*- coding: utf-8 -*-

from sys import argv
import re
import os.path

start = re.compile('beginfig\((\d+)\)')
end = re.compile('endfig')
path = argv[1]
export = argv[2]

char = None
buff = None

with open(path, 'r') as h:
    for line in h:
        if char is None:
            m = start.search(line)

            if m:
                char = int(m.group(1))
                buff = line

        else:
            buff += line
            m = end.search(line)

            if m:
                with open(os.path.join(export, '{}.mp'.format(char)), 'w') as w:
                    w.write(buff)
                    w.close

                char = None
                buff = None
