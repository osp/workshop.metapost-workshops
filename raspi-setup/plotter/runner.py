#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import chiplotle
import optparse
import os.path
import time
import glob

cachedir = "hpgl"

VIRTUAL = False


def compare_cachenames(a, b):
    """
      Paths are expected plotterto have pattern:
      (\d+)-(\d+)\.hpgl
    """
    x = int(os.path.basename(a).split('-', 1)[0])
    y = int(os.path.basename(b).split('-', 1)[0])

    return cmp(x, y)


def get_oldest_cachefile():
    files = [path for path in glob.glob(
        os.path.join(cachedir, '*.hpgl'))]
    if files:
        sorted(files, cmp=compare_cachenames)
        return files[0]
    else:
        return None


def delete_cachefile(path):
    os.unlink(path)


if VIRTUAL:
    plotter = chiplotle.tools.plottertools.instantiate_virtual_plotter()
else:
    plotter = chiplotle.tools.plottertools.instantiate_plotters()[0]


while True:
    cachefile = get_oldest_cachefile()

    if cachefile:
        with open(cachefile, 'r') as h:
            hpgl = h.read()
            try:
                plotter.write('IN;{};'.format(hpgl))
                plotter._serial_port.flush()

                delete_cachefile(cachefile)

                if VIRTUAL:
                    chiplotle.tools.io.view(plotter)
                    plotter = chiplotle.tools.plottertools.instantiate_virtual_plotter()
            except:
                print 'Something went wrong while plotting'
                pass

        time.sleep(4)
