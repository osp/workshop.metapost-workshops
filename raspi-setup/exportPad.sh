baseurl="http://localhost/ether"

if [[ $1 == http* ]];
then
   padurl=$1
else
   padurl="${baseurl}/p/${1}"
fi

if [ $# -gt 1 ];
then
  interval=$2
else
  interval=10
fi

for (( ; ;  ));
do
  echo ""
  echo ""
  echo $padname
  mkdir -p "svg"

  echo 'outputtemplate := "svg/%c.svg";' > "pad.mp"
  curl "${padurl}/export/txt" >> "pad.mp"
  cp pad.mp www/pad.mp
  rm -f "svg/*.svg"
  mpost -interaction=batchmode -s 'outputformat="svg"' "pad.mp"
  cp pad.log www/log.txt
  rm -f "mpost/*.mpost"
  python split_mpost.py pad.mp mpost

  sleep $interval
done