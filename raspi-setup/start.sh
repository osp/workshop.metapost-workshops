#!/bin/bash

# stop other instance if its running by accident
bash stop.sh


if [ $# -gt 0 ]; then
        padname=$1
        echo "Downloading from: ${padname}/export/txt"
else
        echo "No pad specified using default: metafont"
        padname="metafont"
        echo "Downloading from: http://localhost/ether/p/${padname}/export/txt"
fi

if [ $# -gt 1 ]; then
        interval=$2
else
        echo "No interval specified, using default of 5 seconds"
        interval=5
fi

echo "Checking the pad every ${interval} seconds"

# Starting screen process
screen -dmS mpost bash exportPad.sh $padname $interval

#screen -dmS api bash start_api.sh

echo "Started the plotter"

screen -dmS plotter bash start_plotter.sh

#sudo service nginx reload