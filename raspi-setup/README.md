## Requirements
sudo apt-get install texlive inkscape nginx python-pip

pip install chiplotle flask gunicorn

Install etherpad
sqlite

Install nginx conf
sudo cp nginx-conf

## To dialogue with the pi and launch the workshop setup

scan the different IP
nmap -sP 192.168.1.*
connect to the raspberry one
ssh pi@192.168.1.x
psw : raspberry

or using the OSP router
ssh pi@raspberrypi.lan

bash start.sh [padname] [reloadinterval]
1_ download the etherpad content and run the matapost process to convert the ps into svg
2_ (optional) to run the plotter : take the svg to hpgl to a folder where are stocked the drawings to plott

bash stop.sh to stop

to create un pad
http://192.168.1.18/ether/p/nameofyourpad

to visualise your glyph
right click on the preview, select view image

sudo shutdown -h now

## Changing the amount of pens on the plotter
The API is configured to expect 4 pens. When you install more or less pens you'll have to change the settings in the API.

The pen settings (amount, speed, force) are defined in the api-file:
/srv/metaplot-api/api.py

Change with for example nano (use sudo) after the changes are saved restart the api.

## Restarting the API
sudo systemctl restart metaplot-api

## Restart Etherpad
sudo systeamctl restart etherpad-lite